package net.pexao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

public class SnmpSimpleClient {
	
	private Snmp snmp;
	private String address;
	
	public SnmpSimpleClient(String address) {
		this.address = address;
		
		try {
			start();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void start() throws IOException {
		TransportMapping transport = new DefaultUdpTransportMapping();
		
		this.snmp = new Snmp(transport);
		transport.listen();
	}
	
	public void stop() throws IOException {
		snmp.close();
	}

	public PDU getPDU(List<OID> oids) {
		PDU pdu = new PDU();
		
		oids.forEach(oid -> pdu.add(new VariableBinding(oid)));
		
		pdu.setType(PDU.GET);
		return pdu;
	}
	
	public Target getTarget() {
		Address targetAddress = GenericAddress.parse(this.address);
		
		CommunityTarget target = new CommunityTarget();
		
		target.setCommunity(new OctetString("public"));
		target.setAddress(targetAddress);
		target.setRetries(2);
		target.setTimeout(1500);
		target.setVersion(SnmpConstants.version2c);
		
		return target;
	} 
	
	public List<String> getAsString(List<OID> oids) throws IOException {
		List<String> parsedOids = new ArrayList<>();
		
		for(OID oid: oids) {
			ResponseEvent event = snmp.send(getPDU(Arrays.asList(oid)), getTarget(), null);
			parsedOids.add(event.getResponse().get(0).getVariable().toString());
		}
		
		return parsedOids;
	}
}
