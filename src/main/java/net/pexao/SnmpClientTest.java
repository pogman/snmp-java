package net.pexao;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;

public class SnmpClientTest {
	public static void main(String[] args) {
		SnmpSimpleClient client = new SnmpSimpleClient("udp:localhost/161");
		
		List<OID> oids = Arrays.asList(
			SnmpConstants.sysDescr, 
			SnmpConstants.sysLocation, 
			SnmpConstants.sysName,
			SnmpConstants.sysContact,
			SnmpConstants.sysObjectID,
			SnmpConstants.sysServices,
			SnmpConstants.sysUpTime
		);
		
		try {
			client.getAsString(oids).forEach(System.out::println);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
